A compact, dark GTK 2.0 theme. **Clone into your `~/.themes` or `/usr/share/themes` to use.**

IAPTnightmare is a recolor of [iamfuss' GTK 2.0 theme "IAPTo1"](http://fav.me/d2c9e5o), 
which itself is a port of [IAPT by kastoob](http://fav.me/d29etbw).  
It matches the Nightmare and Nightmare-01 Openbox themes (as shown in the sample image).
[Titlebraille](https://gitlab.com/MachineFossil/titlebraille) titlebar icons also shown.

![](IAPTnightmare.png)